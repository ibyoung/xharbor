package org.jocean.xharbor.util;

public interface RulesMXBean {
    public String[] getRoutingRules();
}
