/**
 * 
 */
package org.jocean.xharbor.api;

/**
 * @author isdom
 *
 */
public interface RoutingInfo {
    public String getXRouteCode();
    public String getMethod();
    public String getPath();
}
